<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->group(['middleware' => 'auth:api'], function ($router) {

    /** Hosting Packages */
    $router->resource('site', 'PackageController');
    $router->get('site/share/{siteId}/{add}', 'PackageController@share');

    /** Single Sign On */
    $router->get('sso/{stackUserId}', 'PackageController@sso');

    /** Migrations */
    $router->post('migrate/test', 'MigrationController@test');

    /** Teams */
    $router->resource('team', 'TeamController');
    $router->post('settings/team/{team}/invitations', 'SparkOverrides\MailedInvitationController@store');

});
