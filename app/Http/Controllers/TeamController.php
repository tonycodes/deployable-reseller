<?php

namespace App\Http\Controllers;

use App\Services\HyperClient;

class TeamController extends HyperClient
{
    /**
     * List all hosting packages attached to this token
     */
    public function index()
    {

        return $this->client->get('/api/v1/team');

    }

    /**
     * Get single package by id
     */
    public function show($teamId)
    {

        return $this->client->get('/api/v1/team/' . $teamId);

    }

    /**
     * Create a new package
     */
    public function store()
    {
        $payload = [
            'name' => '',
            'slug' => '',
        ];

        return $this->client->post('/api/v1/team', $payload);

    }

    /**
     * Destroy package
     */
    public function destroy($teamId)
    {

        return $this->client->delete('/api/v1/team/' . $teamId);

    }

}