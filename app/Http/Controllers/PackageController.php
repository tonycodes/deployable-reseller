<?php

namespace App\Http\Controllers;

use App\Services\HyperClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;

class PackageController extends HyperClient
{

    /**
     * List all hosting packages attached to this user
     */
    public function index()
    {

        return $this->get('/api/v1/suser/' . Auth::user()->hyper_host_id . '/package');

    }

    public function store()
    {

        if(Auth::user()->hasRemainingAllowance()) {

            try {

                HyperClient::post('/api/v1/suser/' . Auth::user()->hyper_host_id . '/package', ['json' => request()->except('_token')]);
                $response = 'Hosting package created! Have fun';
                $responseType = 'success';

            } catch (ClientException $e) {

                $response = $e->getResponse()->getBody()->getContents();
                switch ($e->getCode()) {
                    case 400:
                        $responseType = 'warning';
                        break;
                    case 409:
                        $responseType = 'warning';
                        break;
                    case 500:
                        $responseType = 'danger';
                        break;
                    default:
                        $responseType = 'info';

                }

            }

        } else {

            $response = 'Upgrade required, you have used all of your allowance';
            $responseType = 'warning';

        }


        alertBar('', $response, $responseType);

        return redirect(url('/home'));

    }

    /**
     * @param $stackUserId
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sso($packageUserId)
    {

        $sessionLink = $this->get('/api/v1/sso/' . $packageUserId);

        return redirect(url($sessionLink));

    }

}