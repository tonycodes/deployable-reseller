<?php

namespace App\Http\Controllers;

use Facades\App\Http\Controllers\PackageController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $packages = json_decode(PackageController::index());

        return view('home', compact('packages'));

    }
}
