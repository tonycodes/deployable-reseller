<?php

namespace App\Http\Controllers;

use App\Services\HyperClient;

class MigrationController extends HyperClient
{

    /**
     * Start a new migration
     */
    public function store()
    {

        $payload = [
            'source_hostname' => '',
            'source_username' => '',
            'source_password' => '',
            'source_domain' => '',
        ];

        return $this->post('/api/v1/migration', $payload);

    }

}