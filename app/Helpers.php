<?php

use Facades\App\Services\HyperClient;

/**
 * @param $title
 * @param $message
 * @param $type
 */
function alertBar($title, $message, $type)
{

    session()->flash('alert-bar', ['title' => $title, 'message' => $message, 'type' => $type]);

}

function resellerPlans()
{

    return json_decode(HyperClient::get('/api/v1/reseller/plan'));

}