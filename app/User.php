<?php

namespace App;

use Facades\App\Services\HyperClient;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'hyper_host_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @return int
     */
    public function allowance()
    {

        return json_decode( HyperClient::get('/api/v1/suser/' . $this->hyper_host_id ),false )->reseller_plan->allowance;

    }

    /**
     * @return bool
     */
    public function hasRemainingAllowance()
    {

        return count($this->hostingPackages()) < $this->allowance();

    }

    /**
     * @return mixed
     */
    public function hostingPackages()
    {

        return json_decode(HyperClient::get('/api/v1/suser/' . $this->hyper_host_id . '/package'));

    }

}
