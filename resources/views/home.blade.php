@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="card shadow bg-white rounded">

                    <div class="card-body">

                        <form action="/package" method="post" class="form-inline">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group mb-2">
                                <label for="domain">Domain</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" value="" name="domain">
                                </div>
                            </div>

                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Platform</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01" name="platform">
                                    <option value="905">Linux</option>
                                    <option value="907">WordPress Optimised</option>
                                    <option value="21893">Windows 2012</option>
                                </select>
                            </div>

                            <!-- Create Button -->
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-6">
                                    <button type="submit" class="btn btn-primary mb-2">
                                        Add Hosting Package
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <hr>

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <th>Domain</th>
                            <th>Platform</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th></th>
                            </thead>
                            @foreach($packages as $package)
                                <tr>
                                    <td>{{ $package->name }}</td>
                                    <td>{{ $package->packageTypeName }}</td>
                                    <td>{{ $package->created }}</td>
                                    <td>{{ $package->enabled ? 'Active' : 'Disabled' }}</td>
                                    <td>
                                        <a href="{{ route('sso', $package->stackUsers[0] ?? 0) }}"
                                           class="btn btn-sm btn-group-sm btn-primary">Control Panel</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
