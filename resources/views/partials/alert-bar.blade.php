@if(isset($errors) && $errors->any())
    <div class="alert alert-warning mt-0 mb-0 text-center">{{ $errors->first() }}</div>
@endif

@if(session()->has('alert-bar'))
    <div class="alert alert-{{ session('alert-bar')['type'] }} text-center mb-0 mt-0">
        <b>{{ session('alert-bar')['title'] }}</b> {!! session('alert-bar')['message'] !!}
    </div>
@endif